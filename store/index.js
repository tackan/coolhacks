import axios from '~/plugins/axios'
import Vuex from 'vuex'

const store = () =>
  new Vuex.Store({
    state: {
      searchWord: '',
      user: null,
      errFlash: null,
      sucFlash: null,
      actInfoFlash: null,
      actErrFlash: null,
      hasActInfo: false,
      hasActErr: false,
      isNewUser: false,
      newUser: {},
      resetDialog: false,
      currentCools: 0,
      actives: {
        active: 1000,
        active2: 2500
      },
      player: null
    },
    mutations: {
      setWord (state, searchWord) {
        state.searchWord = searchWord
      },
      setUser (state, user) {
        state.user = user
      },
      setError (state, msg) {
        state.errFlash = msg
      },
      setSuccess (state, msg) {
        state.sucFlash = msg
      },
      setActivateInfo (state, msg) {
        state.actInfoFlash = msg
        state.hasActInfo = true
      },
      setActivateErr (state, msg) {
        state.actErrFlash = msg
        state.hasActErr = true
      },
      setHasActivateInfo (state, has) {
        state.hasActInfo = has
      },
      setHasActivateErr (state, has) {
        state.hasActErr = has
      },
      setNewUser (state, user) {
        state.newUser = user
      },
      setIsNewUser (state, is) {
        state.isNewUser = is
      },
      setResetDialogOpened (state, is) {
        state.resetDialog = is
      },
      setCurrentCools (state, num) {
        state.currentCools = num
      }
    },
    actions: {
      nuxtServerInit ({ commit }, { req, res }) {
        let suc = req.flash('success')
        let err = req.flash('error')
        let info = req.flash('info')
        console.log(suc)
        if (err.length > 0) {
          commit('setError', err[0])
        } else if (suc.length > 0) {
          commit('setSuccess', suc[0])
        } else if (info.length > 0) {
          commit('setActivateInfo', info[0])
        }

        if (req.session && req.session.authUser) {
          commit('setUser', req.session.authUser)
        }
        console.log(req.query)
        console.log('res')
        if (req.query && req.query.newUser) {
          commit('setIsNewUser', true)
          commit('setNewUser', req.query)
        }
      },
      logIn (context, user) {
        axios
          .post('http://localhost:3000/api/login', user)
          .then(res => {
            console.log(res.data)
            if (res.data.info) {
              console.log(res.data.info)
              context.commit('setUser', res.data.user)
              context.commit('setActivateInfo', res.data.info)
            } else if (res.data.error) {
              context.commit('setActivateErr', res.data.error)
            }
          })
          .catch(e => {
            console.log(e.message)
          })
      },
      logOut (context, user) {
        axios
          .post('http://localhost:3000/api/logout')
          .then(res => {
            context.commit('setUser', null)
            context.commit('setActivateInfo', res.data.info)
          })
          .catch(e => {
            console.log(e.message)
          })
      },
      signUp (context, user) {
        axios
          .post('http://localhost:3000/api/signup', user)
          .then(res => {
            console.log(res.data)
            if (res.data.info) {
              context.commit('setActivateInfo', res.data.info)
            } else if (res.data.error) {
              context.commit('setActivateErr', res.data.error)
            }
          })
          .catch(e => {
            console.log(e.message)
          })
      },
      oauthActivate (context) {
        let activateUrl
        let newUser = context.state.newUser
        if (newUser.twitter_id) {
          activateUrl = 'http://localhost:3000/api/twitter/activate'
        } else if (newUser.facebook_id) {
          activateUrl = 'http://localhost:3000/api/facebook/activate'
        } else if (newUser.google_id) {
          activateUrl = 'http://localhost:3000/api/google/activate'
        } else if (newUser.github_id) {
          activateUrl = 'http://localhost:3000/api/github/activate'
        }
        axios
          .post(activateUrl, context.state.newUser)
          .then(res => {
            console.log(res.data)
            if (res.data.info) {
              context.commit('setActivateInfo', res.data.info)
              context.commit('setIsNewUser', false)
              context.commit('setNewUser', {})
            } else if (res.data.error) {
              context.commit('setActivateErr', res.data.error)
            }
          })
          .catch(e => {
            console.log(e.message)
          })
      },
      sendResetMail (context, email) {
        axios
          .post('http://localhost:3000/api/forgot', { email: email })
          .then(res => {
            console.log(res.data)
            if (res.data.info) {
              context.commit('setActivateInfo', res.data.info)
              context.commit('setResetDialogOpened', false)
            } else if (res.data.error) {
              context.commit('setActivateErr', res.data.error)
            }
          })
          .catch(e => {
            console.log(e.message)
          })
      },
      resetPassword (context, { postData, response }) {
        axios
          .post('http://localhost:3000/api/reset', postData)
          .then(res => {
            console.log('response')
            if (res.data.info) {
              console.log('password reseted')
              response.success = res.data.info
              // context.commit('setActivateInfo', res.data.info)
              console.log(response)
            } else if (res.data.error) {
              console.log(response)
              response.error = res.data.error
              // context.commit('setActivateErr', res.data.error)
            }
          })
          .catch(e => {
            // context.commit('setActivateErr', e.message)
            // response.push('/')
            response.error = e.message
          })
      },
      postCool (context, { timeId, movieId }) {
        let postData = {
          user_id: context.state.user ? context.state.user.id : null,
          time_id: timeId
        }
        axios
          .post('http://localhost:3000/api/movie/' + movieId + '/cool', {
            cool: postData
          })
          .then(res => {
            console.log(res.data.info)
          })
          .catch(e => {
            console.log(e.message)
          })
      }
    },
    getters: {
      isLoggedIn (state) {
        return !!state.user
      },
      getUser (state) {
        return state.user
      },
      hasError (state) {
        return !!state.errFlash
      },
      hasSuccess (state) {
        return !!state.sucFlash
      },
      getErrorMsg (state) {
        return state.errFlash
      },
      getSuccessMsg (state) {
        return state.sucFlash
      },
      hasActInfo (state) {
        return !!state.actInfoFlash
      },
      hasActErr (state) {
        return !!state.actErrFlash
      },
      getActInfo (state) {
        return state.hasActInfo ? state.actInfoFlash : null
      },
      getActErr (state) {
        return state.hasActErr ? state.actErrFlash : null
      },
      getActives (state) {
        return state.actives
      }
    }
  })

export default store
