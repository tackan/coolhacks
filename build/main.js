require('source-map-support/register')
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__filename, __dirname) {

var fs = __webpack_require__(13);
var path = __webpack_require__(14);
var Sequelize = __webpack_require__(15);
var basename = path.basename(__filename);
var env = "development" || 'development';
// const config = require(__dirname + '/../config/config.json')[env]
var config = __webpack_require__(16)[env];
var db = {};

var sequelize = void 0;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable]);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs.readdirSync(__dirname).filter(function (file) {
  return file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js';
}).forEach(function (file) {
  var p = path.join(__dirname, file);
  if (env === 'development') {
    p = '../' + p;
  }
  var model = sequelize['import'](p);
  db[model.name] = model;
});

Object.keys(db).forEach(function (modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
/* WEBPACK VAR INJECTION */}.call(exports, "models/index.js", "models"))

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("passport");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("bcrypt-nodejs");

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_express__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_express_session__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_express_session___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_express_session__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_cookie_parser__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_cookie_parser___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_cookie_parser__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_body_parser__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_body_parser___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_body_parser__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_morgan__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_morgan___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_morgan__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_passport__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_passport___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_passport__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_connect_flash__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_connect_flash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_connect_flash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_nuxt__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_nuxt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_nuxt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__api__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__config_secret__ = __webpack_require__(21);


// import ejwt from 'express-jwt'










__webpack_require__(22)(__WEBPACK_IMPORTED_MODULE_5_passport___default.a);

var app = __WEBPACK_IMPORTED_MODULE_0_express___default()();
var host = process.env.HOST || '127.0.0.1';
var port = process.env.PORT || 3000;

app.set('port', port);

// set up application
app.use(__WEBPACK_IMPORTED_MODULE_4_morgan___default()('dev'));
app.use(__WEBPACK_IMPORTED_MODULE_2_cookie_parser___default()()); // read cookies (needed for auth)
app.use(__WEBPACK_IMPORTED_MODULE_3_body_parser___default.a.urlencoded({ extended: true }));
app.use(__WEBPACK_IMPORTED_MODULE_3_body_parser___default.a.json());
// app.use(ejwt({ secret: secret.secret, userProperty: 'tokenPayload' }).unless({ path: ['/api/login', '/api/movies/search', 'api/movies/news', 'api/movies/recomended', '/'] }))
app.use(__WEBPACK_IMPORTED_MODULE_1_express_session___default()(__WEBPACK_IMPORTED_MODULE_9__config_secret__["a" /* default */]));
app.use(__WEBPACK_IMPORTED_MODULE_5_passport___default.a.initialize());
app.use(__WEBPACK_IMPORTED_MODULE_5_passport___default.a.session());
app.use(__WEBPACK_IMPORTED_MODULE_6_connect_flash___default()());
// Import API Routes
app.use('/api', __WEBPACK_IMPORTED_MODULE_8__api__["a" /* default */]);

// Import and Set Nuxt.js options
var config = __webpack_require__(29);
config.dev = !("development" === 'production');

// Init Nuxt.js
var nuxt = new __WEBPACK_IMPORTED_MODULE_7_nuxt__["Nuxt"](config);

// Build only in dev mode
if (config.dev) {
  var builder = new __WEBPACK_IMPORTED_MODULE_7_nuxt__["Builder"](nuxt);
  builder.build();
}

// Give nuxt middleware to express
app.use(nuxt.render);

// Listen the server
app.listen(port, host);
console.log('ENV: ' + "development");
console.log('Server listening on ' + host + ':' + port); // eslint-disable-line no-console

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("express-session");

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("cookie-parser");

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("morgan");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("connect-flash");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("nuxt");

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_express__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__movies__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__session__ = __webpack_require__(17);





var router = Object(__WEBPACK_IMPORTED_MODULE_0_express__["Router"])();

// Add USERS Routes
router.use(__WEBPACK_IMPORTED_MODULE_1__movies__["a" /* default */]);
router.use(__WEBPACK_IMPORTED_MODULE_2__session__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (router);

/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_express__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var models = __webpack_require__(1);
var router = Object(__WEBPACK_IMPORTED_MODULE_0_express__["Router"])();

/* GET movies listing. */
router.get('/movies', function (req, res, next) {
  models.movie.findAll({
    include: [{ model: models.user, attributes: ['name', 'avatar'] }, { model: models.tag, attributes: ['id', 'tag_name'] }],
    limit: 12,
    offset: 0
  }).then(function (m) {
    res.json(m);
  }).catch(function (e) {
    res.json(e.meesage);
  });
});

/* GET movies/search listing. */
router.get('/movies/search', function (req, res, next) {
  var word = '';
  var limit = void 0;
  var offset = void 0;
  if (req.query.search_word) {
    word = req.query.search_word;
  }

  if (req.query.begin) {
    offset = req.query.begin > 0 ? req.query.begin - 1 : 0;
  } else {
    offset = 0;
  }

  if (req.query.limit) {
    limit = parseInt(req.query.limit);
    limit = limit > 0 ? limit : 12;
  } else {
    limit = 12;
  }

  models.movie.findAll({
    include: [{
      model: models.user,
      attributes: ['id', 'name', 'avatar']
    }, {
      model: models.tag,
      attributes: ['id', 'tag_name']
    }],
    limit: limit,
    offset: offset,
    where: _defineProperty({}, models.Sequelize.Op.or, [{
      title: _defineProperty({}, models.Sequelize.Op.iLike, '%' + word + '%')
    }, {
      description: _defineProperty({}, models.Sequelize.Op.iLike, '%' + word + '%')
    }]),
    order: [['created_at', 'DESC']]
  }).then(function (movies) {
    res.json(movies);
  });
});

/* GET movies/recomended listing. */
router.get('/movies/recomendeds', function (req, res, next) {
  var limit = void 0;
  var offset = void 0;
  if (req.query.begin) {
    offset = req.query.begin > 0 ? req.query.begin - 1 : 0;
  } else {
    offset = 0;
  }
  if (req.query.limit) {
    limit = parseInt(req.query.limit);
    limit = limit > 0 ? limit : 12;
  } else {
    limit = 12;
  }

  models.movie.findAll({
    include: [{ model: models.user, attributes: ['name', 'avatar'] }, { model: models.tag, attributes: ['id', 'tag_name'] }],
    limit: limit,
    offset: offset,
    order: [['w_count', 'DESC'], ['c_count', 'DESC']]
  }).then(function (m) {
    res.json(m);
  }).catch(function (e) {
    res.json(e.meesage);
  });
});

/* GET movies/recomended listing. */
router.get('/movies/news', function (req, res, next) {
  var limit = void 0;
  var offset = void 0;
  if (req.query.begin) {
    offset = req.query.begin > 0 ? req.query.begin - 1 : 0;
  } else {
    offset = 0;
  }
  if (req.query.limit) {
    limit = parseInt(req.query.limit);
    limit = limit > 0 ? limit : 12;
  } else {
    limit = 12;
  }

  models.movie.findAll({
    include: [{ model: models.user, attributes: ['name', 'avatar'] }, { model: models.tag, attributes: ['id', 'tag_name'] }],
    limit: limit,
    offset: offset,
    order: [['created_at', 'DESC']]
  }).then(function (m) {
    res.json(m);
  }).catch(function (e) {
    res.json(e.meesage);
  });
});

/* GET movie by ID. */
router.get('/movie/:id', function (req, res, next) {
  var id = parseInt(req.params.id);
  if (id) {
    models.movie.findOne({
      where: { id: id },
      include: [{
        model: models.user,
        attributes: ['name', 'avatar']
      }, {
        model: models.tag,
        attributes: ['id', 'tag_name']
      }, {
        model: models.comment,
        attributes: ['id', 'text', 'timing', 'is_question', 'goods'],
        include: [{
          model: models.answer,
          attributes: ['id', 'text', 'question_id', 'updated_at'],
          include: [{
            model: models.user,
            attributes: ['name', 'avatar']
          }]
        }, {
          model: models.user,
          attributes: ['name', 'avatar']
        }]
      }],
      order: [[models.comment, 'timing', 'ASC'], [models.comment, models.answer, 'updated_at', 'ASC']]
    }).then(function (m) {
      res.json(m);
    }).catch(function (e) {
      console.log(e.message);
      res.json(e.meesage);
    });
  } else {
    res.sendStatus(404);
  }
});

router.get('/movie/:id/comments', function (req, res, next) {
  var id = parseInt(req.params.id);
  if (id) {
    models.comment.findAll({
      where: { movie_id: id },
      attributes: ['id', 'text', 'timing', 'is_question', 'goods'],
      include: [{
        model: models.answer,
        attributes: ['id', 'text', 'question_id', 'updated_at'],
        include: [{
          model: models.user,
          attributes: ['name', 'avatar']
        }]
      }, {
        model: models.user,
        attributes: ['name', 'avatar']
      }],
      order: [['timing', 'ASC'], [models.answer, 'updated_at', 'ASC']]
    }).then(function (m) {
      console.log(m);
      res.json(m);
    }).catch(function (e) {
      res.json(e.message);
    });
  } else {
    res.sendStatus(404);
  }
});

router.post('/movie/:id/comment', function (req, res, next) {
  var movieId = parseInt(req.params.id);
  req.body.text = req.body.text.replace(/^\s+|\s+$/g, '');
  if (movieId && req.session.authUser && req.session.authUser.id === req.body.user_id && req.body.text) {
    var comment = req.body;
    comment.movie_id = movieId;
    models.comment.create(comment).then(function (m) {
      models.comment.findAll({
        where: { movie_id: movieId },
        include: [{
          model: models.answer,
          attributes: ['text', 'question_id', 'updated_at'],
          include: [{
            model: models.user,
            attributes: ['name', 'avatar']
          }]
        }, {
          model: models.user,
          attributes: ['name', 'avatar']
        }],
        order: [['timing', 'ASC'], [models.answer, 'updated_at', 'ASC']]
      }).then(function (m) {
        res.json(m);
      }).catch(function (e) {
        res.json(e.message);
      });
    }).catch(function (e) {
      res.json(e.message);
    });
  } else {
    models.comment.findAll({
      where: { movie_id: movieId },
      include: [{
        model: models.answer,
        attributes: ['text', 'question_id', 'updated_at'],
        include: [{
          model: models.user,
          attributes: ['name', 'avatar']
        }]
      }, {
        model: models.user,
        attributes: ['name', 'avatar']
      }],
      order: [['timing', 'ASC'], [models.answer, 'updated_at', 'ASC']]
    }).then(function (m) {
      res.json(m);
    }).catch(function (e) {
      res.json(e.message);
    });
  }
});

router.get('/movie/:id/cools', function (req, res, next) {
  var id = parseInt(req.params.id);
  if (id) {
    models.coolaggregate.findAll({
      where: { id: id },
      attributes: ['time_id', 'count'],
      order: [['time_id', 'ASC']]
    }).then(function (m) {
      res.json(m);
    }).catch(function (e) {
      res.json(e.meesage);
    });
  } else {
    res.sendStatus(404);
  }
});

router.get('/movie/:id/nexts', function (req, res, next) {
  // タグが一致していて且つ閲覧数とcoolカウント上位20件
  var id = parseInt(req.params.id);
  if (id) {
    models.movie.findOne({
      where: {
        id: id
      },
      attributes: [],
      include: [{
        model: models.tag,
        attributes: ['id']
      }]
    }).then(function (m) {
      var ids = m.tags.map(function (tag) {
        return tag.id;
      });
      models.movie.findAll({
        attributes: ['id', 'w_count', 'c_count'],
        include: [{
          model: models.tag,
          attributes: ['id'],
          where: {
            id: _defineProperty({}, models.Sequelize.Op.in, ids)
          }
        }],
        where: {
          id: _defineProperty({}, models.Sequelize.Op.not, id)
        },
        limit: 20,
        offset: 0,
        order: [['w_count', 'DESC'], ['c_count', 'DESC']]
      }).then(function (idRes) {
        console.log(idRes);
        var mIds = idRes.map(function (val) {
          return val.id;
        });
        models.movie.findAll({
          include: [{
            model: models.user,
            attributes: ['name', 'avatar']
          }, {
            model: models.tag,
            attributes: ['id', 'tag_name']
          }],
          where: {
            id: _defineProperty({}, models.Sequelize.Op.in, mIds)
          },
          limit: 20,
          offset: 0,
          order: [['w_count', 'DESC'], ['c_count', 'DESC']]
        }).then(function (result) {
          console.log(result);
          res.json(result);
        });
      });
    }).catch(function (e) {
      res.json(e.meesage);
    });
  } else {
    res.sendStatus(404);
  }
});

router.post('/movie/:id/cool', function (req, res, next) {
  var cool = req.body.cool;
  cool.movie_id = req.params.id;
  if (cool.user_id) {
    console.log('LOGGEDIN USER');
  } else {
    console.log('GUEST USER');
  }
  models.cool.create(cool).then(function (result) {
    res.json({ info: 'Cool posted!' });
  }).catch(function (e) {
    res.json({ error: e.message });
  });
});

router.post('/movie/:id/answer', function (req, res, next) {
  var movieId = parseInt(req.params.id);
  req.body.text = req.body.text.replace(/^\s+|\s+$/g, '');
  if (movieId && req.session.authUser && req.session.authUser.id === req.body.user_id && req.body.text) {
    var answer = req.body;
    answer.movie_id = movieId;
    models.answer.create(answer).then(function (m) {
      models.comment.findAll({
        where: { movie_id: movieId },
        include: [{
          model: models.answer,
          attributes: ['text', 'question_id', 'updated_at'],
          include: [{
            model: models.user,
            attributes: ['name', 'avatar']
          }],
          order: [['updated_at', 'ASC']]
        }, {
          model: models.user,
          attributes: ['name', 'avatar']
        }],
        order: [['timing', 'ASC'], [models.answer, 'updated_at', 'ASC']]
      }).then(function (m) {
        res.json(m);
      }).catch(function (e) {
        res.json(e.message);
      });
    }).catch(function (e) {
      res.json(e.message);
    });
  } else {
    models.comment.findAll({
      where: { movie_id: movieId },
      include: [{
        model: models.answer,
        attributes: ['text', 'question_id', 'updated_at'],
        include: [{
          model: models.user,
          attributes: ['name', 'avatar']
        }],
        order: [['updated_at', 'ASC']]
      }, {
        model: models.user,
        attributes: ['name', 'avatar']
      }],
      order: [['timing', 'ASC'], [models.answer, 'updated_at', 'ASC']]
    }).then(function (m) {
      res.json(m);
    }).catch(function (e) {
      res.json(e.message);
    });
  }
});

/* harmony default export */ __webpack_exports__["a"] = (router);

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("sequelize");

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = {"development":{"username":"coolhacks","password":"detsp3932P2bj3g63","database":"cool_hacks_db_dev","host":"127.0.0.1","dialect":"postgres"},"test":{"username":"coolhacks","password":"detsp3932P2bj3g63","database":"database_test","host":"127.0.0.1","dialect":"postgres","ssl":true,"dialectOptions":{"ssl":true}},"production":{"username":"coolhacks","password":"detsp3932P2bj3g63","database":"database_production","host":"127.0.0.1","dialect":"postgres","ssl":true,"dialectOptions":{"ssl":true}}}

/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_passport__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_passport___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_passport__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_express__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_express__);


var mailer = __webpack_require__(18);
var url = __webpack_require__(20);
var router = Object(__WEBPACK_IMPORTED_MODULE_1_express__["Router"])();
var bcrypt = __webpack_require__(3);

var User = __webpack_require__(1).user;

// ====================
// ====== Local =======
// ====================

router.get('/signup/:id/:otp', function (req, res, next) {
  User.findOne({ where: { id: req.params.id, otp: req.params.otp, is_activated: 0 } }).then(function (user) {
    if (!user) {
      req.flash('error', '無効なリンクです。初めからやり直してください');
      res.redirect('/');
    } else {
      if (user.updated_at.getTime() + 1000 * 60 * 60 * 24 <= new Date().getTime()) {
        req.flash('error', 'すでに無効になっているリンクです。初めからやり直してください');
        res.redirect('/');
      } else {
        user.update({ is_activated: 1, updated_at: new Date(), otp: null }).then(function () {
          var authUser = {
            id: user.id,
            name: user.name,
            avatar: user.avatar
          };
          req.session.authUser = authUser;
          req.flash('success', '本登録完了！');
          res.redirect('/');
        });
      }
    }
  }).catch(function (err) {
    console.log(err.message);
    req.flash('error', '無効なリンクです。初めからやり直してください');
    res.redirect('/');
  });
});

router.post('/signup', function (req, res, next) {
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('local-signup', function (err, user, info) {
    if (err) {
      return res.json({
        error: err.message
        // activateMailSended: false
      });
    }
    if (!user) {
      return res.json({
        error: req.flash('error')[0]
        // activateMailSended: false
      });
    } else {
      // console.log(mailer.sendActivateMessage)
      mailer.sendActivateMessage(user, function (success) {
        if (success) {
          return res.json({
            info: '本登録用メールを送信しました。\nメールに記載されているリンクから登録を完了させてください'
            // activateMailSended: false
          });
        } else {
          return res.json({
            error: 'メールを送信できませんでした。もう一度試してみてください'
            // activateMailSended: false
          });
        }
      });
    }
  })(req, res, next);
});

router.post('/login', function (req, res, next) {
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('local-login', function (err, user, info) {
    if (err) {
      return res.json({
        error: err.message
      });
    } else if (!user) {
      return res.json({
        error: 'メールアドレスかパスワードが正しくありません'
      });
    } else {
      req.session.authUser = user;

      return res.json({
        info: 'ログインしました',
        user: user
      });
    }
  })(req, res, next);
});

// ====================
// ===== Twitter ======
// ====================

router.get('/callback/twitter', function (req, res, next) {
  console.log('twitter callback0');
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('twitter-login', function (err, user, info) {
    console.log('twitter callback');
    if (err) {
      console.log(err.message);
      return res.json({
        error: err.message
      });
    } else if (!user) {
      return res.json({
        error: 'ログインエラー。twitterアカウントが見つかりませんでした'
      });
    } else {
      if (user.deactivated) {
        console.log(user);
        return res.redirect(url.format({
          pathname: req.session.current_path,
          query: user
        }));
        // let path = `${req.session.current_path}?id=${user.id}&name=${user.name}&email=${user.email}&newUser=${true}`
        // return res.redirect(path)
      } else {
        req.session.authUser = user;
        req.flash('info', 'ログインしました。');
        return res.redirect(req.session.current_path);
      }
      // return res.json(user)
    }
  })(req, res, next);
});

router.get('/twitter', function (req, res, next) {
  console.log('twitter0');
  req.session.current_path = req.query.current_path;
  console.log(req.query);
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('twitter-login', function (err, user, info) {
    console.log('twitter');
    if (err) {
      console.log(err.message);
      return res.json({
        error: err.message
      });
    }
  })(req, res, next);
});

router.post('/twitter/activate', function (req, res, next) {
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('twitter-signup', function (err, user, info) {
    if (err) {
      return res.json({
        error: err.message
        // activateMailSended: false
      });
    }
    if (!user) {
      return res.json({
        error: req.flash('error')[0]
        // activateMailSended: false
      });
    } else {
      // console.log(mailer.sendActivateMessage)
      mailer.sendActivateMessageTwitter(user, function (success) {
        if (success) {
          return res.json({
            info: '本登録用メールを送信しました。\nメールに記載されているリンクから登録を完了させてください'
            // activateMailSended: false
          });
        } else {
          return res.json({
            error: 'メールを送信できませんでした。もう一度試してみてください'
            // activateMailSended: false
          });
        }
      });
    }
  })(req, res, next);
});

router.get('/twitter/activate/:id/:otp', function (req, res, next) {
  User.findOne({ where: { id: req.params.id, otp: req.params.otp, is_activated: 0 } }).then(function (user) {
    if (!user) {
      req.flash('error', '無効なリンクです。初めからやり直してください');
      res.redirect('/');
    } else {
      if (user.updated_at.getTime() + 1000 * 60 * 60 * 24 <= new Date().getTime()) {
        req.flash('error', 'すでに無効になっているリンクです。初めからやり直してください');
        res.redirect('/');
      } else {
        user.update({ is_activated: 1, updated_at: new Date(), otp: null }).then(function () {
          var authUser = {
            id: user.id,
            name: user.name,
            avatar: user.avatar
          };
          req.session.authUser = authUser;
          req.flash('success', '本登録完了！');
          res.redirect('/');
        });
      }
    }
  }).catch(function (err) {
    console.log(err.message);
    req.flash('error', '無効なリンクです。初めからやり直してください');
    res.redirect('/');
  });
});

// ====================
// ===== Facebook =====
// ====================
router.get('/callback/facebook', function (req, res, next) {
  console.log('facebook callback0');
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('facebook-login', function (err, user, info) {
    console.log('facebook callback');
    if (err) {
      console.log(err.message);
      return res.json({
        error: err.message
      });
    } else if (!user) {
      return res.json({
        error: 'ログインエラー。facebookアカウントが見つかりませんでした'
      });
    } else {
      if (user.deactivated) {
        console.log(user);
        return res.redirect(url.format({
          pathname: req.session.current_path,
          query: user
        }));
      } else {
        req.session.authUser = user;
        req.flash('info', 'ログインしました。');
        return res.redirect(req.session.current_path);
      }
    }
  })(req, res, next);
});

router.get('/facebook', function (req, res, next) {
  console.log('facebook0');
  req.session.current_path = req.query.current_path;
  console.log(req.query);
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('facebook-login', function (err, user, info) {
    console.log('facebook');
    if (err) {
      console.log(err.message);
      return res.json({
        error: err.message
      });
    }
  })(req, res, next);
});

router.post('/facebook/activate', function (req, res, next) {
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('facebook-signup', function (err, user, info) {
    if (err) {
      return res.json({
        error: err.message
        // activateMailSended: false
      });
    }
    if (!user) {
      return res.json({
        error: req.flash('error')[0]
        // activateMailSended: false
      });
    } else {
      // console.log(mailer.sendActivateMessage)
      mailer.sendActivateMessageFacebook(user, function (success) {
        if (success) {
          return res.json({
            info: '本登録用メールを送信しました。\nメールに記載されているリンクから登録を完了させてください'
            // activateMailSended: false
          });
        } else {
          return res.json({
            error: 'メールを送信できませんでした。もう一度試してみてください'
            // activateMailSended: false
          });
        }
      });
    }
  })(req, res, next);
});

router.get('/facebook/activate/:id/:otp', function (req, res, next) {
  User.findOne({ where: { id: req.params.id, otp: req.params.otp, is_activated: 0 } }).then(function (user) {
    if (!user) {
      req.flash('error', '無効なリンクです。初めからやり直してください');
      res.redirect('/');
    } else {
      if (user.updated_at.getTime() + 1000 * 60 * 60 * 24 <= new Date().getTime()) {
        req.flash('error', 'すでに無効になっているリンクです。初めからやり直してください');
        res.redirect('/');
      } else {
        user.update({ is_activated: 1, updated_at: new Date(), otp: null }).then(function () {
          var authUser = {
            id: user.id,
            name: user.name,
            avatar: user.avatar
          };
          req.session.authUser = authUser;
          req.flash('success', '本登録完了！');
          res.redirect('/');
        });
      }
    }
  }).catch(function (err) {
    console.log(err.message);
    req.flash('error', '無効なリンクです。初めからやり直してください');
    res.redirect('/');
  });
});

// ====================
// ====== Google ======
// ====================
router.get('/callback/google', function (req, res, next) {
  console.log('google callback0');
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('google-login', function (err, user, info) {
    console.log('google callback');
    if (err) {
      console.log(err.message);
      return res.json({
        error: err.message
      });
    } else if (!user) {
      return res.json({
        error: 'ログインエラー。googleアカウントが見つかりませんでした'
      });
    } else {
      if (user.deactivated) {
        console.log(user);
        return res.redirect(url.format({
          pathname: req.session.current_path,
          query: user
        }));
      } else {
        req.session.authUser = user;
        req.flash('info', 'ログインしました。');
        return res.redirect(req.session.current_path);
      }
    }
  })(req, res, next);
});

router.get('/google', function (req, res, next) {
  console.log('google0');
  req.session.current_path = req.query.current_path;
  console.log(req.query);
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('google-login', { scope: ['https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/plus.profile.emails.read'] }, function (err, user, info) {
    console.log('google');
    if (err) {
      console.log(err.message);
      return res.json({
        error: err.message
      });
    }
  })(req, res, next);
});

router.post('/google/activate', function (req, res, next) {
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('google-signup', function (err, user, info) {
    if (err) {
      return res.json({
        error: err.message
        // activateMailSended: false
      });
    }
    if (!user) {
      return res.json({
        error: req.flash('error')[0]
        // activateMailSended: false
      });
    } else {
      // console.log(mailer.sendActivateMessage)
      mailer.sendActivateMessageGoogle(user, function (success) {
        if (success) {
          return res.json({
            info: '本登録用メールを送信しました。\nメールに記載されているリンクから登録を完了させてください'
            // activateMailSended: false
          });
        } else {
          return res.json({
            error: 'メールを送信できませんでした。もう一度試してみてください'
            // activateMailSended: false
          });
        }
      });
    }
  })(req, res, next);
});

router.get('/google/activate/:id/:otp', function (req, res, next) {
  User.findOne({ where: { id: req.params.id, otp: req.params.otp, is_activated: 0 } }).then(function (user) {
    if (!user) {
      req.flash('error', '無効なリンクです。初めからやり直してください');
      res.redirect('/');
    } else {
      if (user.updated_at.getTime() + 1000 * 60 * 60 * 24 <= new Date().getTime()) {
        req.flash('error', 'すでに無効になっているリンクです。初めからやり直してください');
        res.redirect('/');
      } else {
        user.update({ is_activated: 1, updated_at: new Date(), otp: null }).then(function () {
          var authUser = {
            id: user.id,
            name: user.name,
            avatar: user.avatar
          };
          req.session.authUser = authUser;
          req.flash('success', '本登録完了！');
          res.redirect('/');
        });
      }
    }
  }).catch(function (err) {
    console.log(err.message);
    req.flash('error', '無効なリンクです。初めからやり直してください');
    res.redirect('/');
  });
});

// ====================
// ====== Github ======
// ====================
router.get('/callback/github', function (req, res, next) {
  console.log('github callback0');
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('github-login', function (err, user, info) {
    console.log('github callback');
    if (err) {
      console.log(err.message);
      return res.json({
        error: err.message
      });
    } else if (!user) {
      return res.json({
        error: 'ログインエラー。githubアカウントが見つかりませんでした'
      });
    } else {
      if (user.deactivated) {
        console.log(user);
        return res.redirect(url.format({
          pathname: req.session.current_path,
          query: user
        }));
      } else {
        req.session.authUser = user;
        req.flash('info', 'ログインしました。');
        return res.redirect(req.session.current_path);
      }
    }
  })(req, res, next);
});

router.get('/github', function (req, res, next) {
  console.log('github0');
  req.session.current_path = req.query.current_path;
  console.log(req.query);
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('github-login', { scope: ['user:email'] }, function (err, user, info) {
    console.log('github');
    if (err) {
      console.log(err.message);
      return res.json({
        error: err.message
      });
    }
  })(req, res, next);
});

router.post('/github/activate', function (req, res, next) {
  __WEBPACK_IMPORTED_MODULE_0_passport___default.a.authenticate('github-signup', function (err, user, info) {
    if (err) {
      return res.json({
        error: err.message
        // activateMailSended: false
      });
    }
    if (!user) {
      return res.json({
        error: req.flash('error')[0]
        // activateMailSended: false
      });
    } else {
      // console.log(mailer.sendActivateMessage)
      mailer.sendActivateMessageGithub(user, function (success) {
        if (success) {
          return res.json({
            info: '本登録用メールを送信しました。\nメールに記載されているリンクから登録を完了させてください'
            // activateMailSended: false
          });
        } else {
          return res.json({
            error: 'メールを送信できませんでした。もう一度試してみてください'
            // activateMailSended: false
          });
        }
      });
    }
  })(req, res, next);
});

router.get('/github/activate/:id/:otp', function (req, res, next) {
  User.findOne({ where: { id: req.params.id, otp: req.params.otp, is_activated: 0 } }).then(function (user) {
    if (!user) {
      req.flash('error', '無効なリンクです。初めからやり直してください');
      res.redirect('/');
    } else {
      if (user.updated_at.getTime() + 1000 * 60 * 60 * 24 <= new Date().getTime()) {
        req.flash('error', 'すでに無効になっているリンクです。初めからやり直してください');
        res.redirect('/');
      } else {
        user.update({ is_activated: 1, updated_at: new Date(), otp: null }).then(function () {
          var authUser = {
            id: user.id,
            name: user.name,
            avatar: user.avatar
          };
          req.session.authUser = authUser;
          req.flash('success', '本登録完了！');
          res.redirect('/');
        });
      }
    }
  }).catch(function (err) {
    console.log(err.message);
    req.flash('error', '無効なリンクです。初めからやり直してください');
    res.redirect('/');
  });
});

// ====================
// == Password Reset ==
// ====================

router.post('/forgot', function (req, res, next) {
  console.log(req.body);
  if (!req.body.email) {
    // res.json({
    //   error: '通信エラー'
    // })
    req.flash('error', '通信エラー');
  } else {
    console.log(req.body.email);
    User.findOne({ where: { email: req.body.email, is_activated: 1 } }).then(function (user) {
      console.log(user);
      if (!user) {
        return res.json({
          error: '登録されていないメールアドレスです'
        });
      } else if (!user.password) {
        var errMsg = void 0;
        if (user.twitter_id) {
          errMsg = 'すでにTwitterアカウントでの登録があります';
        } else if (user.facebook_id) {
          errMsg = 'すでにFacebookアカウントでの登録があります';
        } else if (user.google_id) {
          errMsg = 'すでにGoogleアカウントでの登録があります';
        } else if (user.github_id) {
          errMsg = 'すでにGithubアカウントでの登録があります';
        }
        return res.json({
          error: errMsg
        });
      } else {
        console.log('user exist');
        console.log(user);
        var token = Math.random().toString(36).slice(-30);
        var expire = new Date();
        expire.setHours(expire.getHours() + 3);
        return user.update({ reset_pass_token: token, reset_pass_expire: expire }).then(function () {
          console.log('update');
          var userForMail = {
            id: user.id,
            name: user.name,
            email: user.email,
            reset_pass_token: token
          };
          mailer.sendPasswordResetMessage(userForMail, function (success) {
            if (success) {
              console.log('success');
              return res.json({
                info: 'パスワードリセット用メールを送信しました。\nメールに記載されているリンクから再登録を完了させてください'
                // activateMailSended: false
              });
            } else {
              console.log('false');
              return res.json({
                error: 'メールを送信できませんでした。もう一度試してみてください'
                // activateMailSended: false
              });
            }
          });
        });
      }
    }).catch(function (err) {
      return res.json({ error: err.message });
    });
  }
});

router.post('/reset', function (req, res, next) {
  console.log(req.body);
  User.findOne({ where: { id: parseInt(req.body.id), reset_pass_token: req.body.reset_pass_token } }).then(function (user) {
    if (!user) {
      console.log('no user');
      res.json({
        error: '無効なリンクです。初めからやり直してください'
      });
      // req.flash('error', '無効なリンクです。初めからやり直してください')
      // res.json({})
    } else {
      if (user.reset_pass_expire.getTime() <= new Date().getTime()) {
        console.log('invalid link');
        res.json({
          error: 'すでに無効になっているリンクです。初めからやり直してください'
        });
        // req.flash('error', 'すでに無効になっているリンクです。初めからやり直してください')
        // res.json({})
      } else {
        console.log('user exist');
        user.update({ password: bcrypt.hashSync(req.body.password, null, null), updated_at: new Date(), reset_pass_token: null, reset_pass_expire: null }).then(function () {
          res.json({
            info: 'パスワードの再設定が完了しました。元のページに戻ってログインしてください'
          });
          // req.flash('info', 'パスワードの再設定が完了しました。元のページに戻ってログインしてください')
          // res.json({})
        });
      }
    }
  }).catch(function (err) {
    console.log(err.message);
    res.json({
      error: '無効なリンクです。初めからやり直してください'
    });
    // req.flash('error', '無効なリンクです。初めからやり直してください')
    // res.json({})
  });
});

router.post('/logout', function (req, res, next) {
  delete req.session.authUser;
  res.json({ info: 'ログアウトしました' });
});

/* harmony default export */ __webpack_exports__["a"] = (router);

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

var mailer = __webpack_require__(19);
var transporter = mailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465,
  secure: true, // SSL
  auth: {
    user: 'coolhacks.letter@gmail.com',
    pass: 'xovohccbsuzvyzja'
  }
});

var msgActivate = {
  from: 'Cool Hacks <no-reply@coolhacks.com>',
  to: '',
  subject: 'Cool Hacks 本登録用メール(送信専用)',
  html: ''
};

var msgReset = {
  from: 'Cool Hacks <no-reply@coolhacks.com>',
  to: '',
  subject: 'Cool Hacks パスワードリセット用メール(送信専用)',
  html: ''
};

module.exports = {
  sendActivateMessage: function sendActivateMessage(user, sendAfter) {
    var mailLink = 'http://localhost:3000/api/signup/' + user.id + '/' + user.otp;
    var html = '<h3>Welcome to Cool Hacks!</h3><br><p>\u4E0B\u8A18\u306EURL\u304B\u308924\u6642\u9593\u4EE5\u5185\u306B\u672C\u767B\u9332\u3092\u5B8C\u4E86\u3055\u305B\u3066\u304F\u3060\u3055\u3044</p><br><a href="' + mailLink + '">' + mailLink + '</a>';
    msgActivate.to = user.name + ' <' + user.email + '>';
    msgActivate.html = html;

    transporter.sendMail(msgActivate, function (error, success) {
      if (error) {
        console.log(error);
        sendAfter(false);
      } else {
        console.log(success);
        sendAfter(true);
      }
    });
  },

  sendActivateMessageTwitter: function sendActivateMessageTwitter(user, sendAfter) {
    var mailLink = 'http://localhost:3000/api/twitter/activate/' + user.id + '/' + user.otp;
    var html = '<h3>Welcome to Cool Hacks!</h3><br><p>\u4E0B\u8A18\u306EURL\u304B\u308924\u6642\u9593\u4EE5\u5185\u306B\u672C\u767B\u9332\u3092\u5B8C\u4E86\u3055\u305B\u3066\u304F\u3060\u3055\u3044</p><br><a href="' + mailLink + '">' + mailLink + '</a>';
    msgActivate.to = user.name + ' <' + user.email + '>';
    msgActivate.html = html;

    transporter.sendMail(msgActivate, function (error, success) {
      if (error) {
        console.log(error);
        sendAfter(false);
      } else {
        console.log(success);
        sendAfter(true);
      }
    });
  },

  sendActivateMessageFacebook: function sendActivateMessageFacebook(user, sendAfter) {
    var mailLink = 'http://localhost:3000/api/facebook/activate/' + user.id + '/' + user.otp;
    var html = '<h3>Welcome to Cool Hacks!</h3><br><p>\u4E0B\u8A18\u306EURL\u304B\u308924\u6642\u9593\u4EE5\u5185\u306B\u672C\u767B\u9332\u3092\u5B8C\u4E86\u3055\u305B\u3066\u304F\u3060\u3055\u3044</p><br><a href="' + mailLink + '">' + mailLink + '</a>';
    msgActivate.to = user.name + ' <' + user.email + '>';
    msgActivate.html = html;

    transporter.sendMail(msgActivate, function (error, success) {
      if (error) {
        console.log(error);
        sendAfter(false);
      } else {
        console.log(success);
        sendAfter(true);
      }
    });
  },

  sendActivateMessageGoogle: function sendActivateMessageGoogle(user, sendAfter) {
    var mailLink = 'http://localhost:3000/api/google/activate/' + user.id + '/' + user.otp;
    var html = '<h3>Welcome to Cool Hacks!</h3><br><p>\u4E0B\u8A18\u306EURL\u304B\u308924\u6642\u9593\u4EE5\u5185\u306B\u672C\u767B\u9332\u3092\u5B8C\u4E86\u3055\u305B\u3066\u304F\u3060\u3055\u3044</p><br><a href="' + mailLink + '">' + mailLink + '</a>';
    msgActivate.to = user.name + ' <' + user.email + '>';
    msgActivate.html = html;

    transporter.sendMail(msgActivate, function (error, success) {
      if (error) {
        console.log(error);
        sendAfter(false);
      } else {
        console.log(success);
        sendAfter(true);
      }
    });
  },

  sendActivateMessageGithub: function sendActivateMessageGithub(user, sendAfter) {
    var mailLink = 'http://localhost:3000/api/github/activate/' + user.id + '/' + user.otp;
    var html = '<h3>Welcome to Cool Hacks!</h3><br><p>\u4E0B\u8A18\u306EURL\u304B\u308924\u6642\u9593\u4EE5\u5185\u306B\u672C\u767B\u9332\u3092\u5B8C\u4E86\u3055\u305B\u3066\u304F\u3060\u3055\u3044</p><br><a href="' + mailLink + '">' + mailLink + '</a>';
    msgActivate.to = user.name + ' <' + user.email + '>';
    msgActivate.html = html;

    transporter.sendMail(msgActivate, function (error, success) {
      if (error) {
        console.log(error);
        sendAfter(false);
      } else {
        console.log(success);
        sendAfter(true);
      }
    });
  },

  sendPasswordResetMessage: function sendPasswordResetMessage(user, sendAfter) {
    var mailLink = 'http://localhost:3000/reset?id=' + user.id + '&token=' + user.reset_pass_token;
    var html = '<h3>Welcome to Cool Hacks!</h3><br><p>\u4E0B\u8A18\u306EURL\u304B\u3089\u30D1\u30B9\u30EF\u30FC\u30C9\u3092\u518D\u8A2D\u5B9A\u3057\u3066\u304F\u3060\u3055\u3044</p><br><a href="' + mailLink + '">' + mailLink + '</a>';
    msgReset.to = user.name + ' <' + user.email + '>';
    msgReset.html = html;

    transporter.sendMail(msgReset, function (error, success) {
      if (error) {
        console.log(error);
        sendAfter(false);
      } else {
        console.log(success);
        sendAfter(true);
      }
    });
  }

};

/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = require("nodemailer");

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var session = {
  secret: 'aeavoaenpvaeutpp',
  resave: true,
  saveUninitialized: true,
  cookie: { maxAge: 60 * 60 * 24 * 30 }
};

/* harmony default export */ __webpack_exports__["a"] = (session);

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

var LocalStrategy = __webpack_require__(23).Strategy;
var TwitterStrategy = __webpack_require__(24).Strategy;
var FacebookStrategy = __webpack_require__(25).Strategy;
var GoogleStrategy = __webpack_require__(26).OAuth2Strategy;
var GithubStrategy = __webpack_require__(27).Strategy;
var straConfig = __webpack_require__(28);
var bcrypt = __webpack_require__(3);

var User = __webpack_require__(1).user;

module.exports = function (passport) {
  // used to serialize the user for the session
  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  // used to deserialize the user
  passport.deserializeUser(function (id, done) {
    User.findOne({ where: { id: id } }).then(function (user) {
      done(null, user);
    }).catch(function (err) {
      done(err, null);
    });
  });

  // LOCAL SIGNUP

  passport.use('local-signup', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true // allows us to pass back the entire request to the callback
  }, function (req, email, password, done) {
    User.findOne({ where: { email: email } }).then(function (user) {
      if (user) {
        // 既に同じemailで本登録されているユーザレコードがある場合
        if (user.is_activated === 1) {
          return done(null, false, req.flash('error', 'このメールアドレスは既に使われています'));
        } else if (user.is_activated === 0) {
          return User.findOne({ where: { name: req.body.name } }).then(function (user2) {
            if (!!user2 && user2.email !== email) {
              return done(null, false, req.flash('error', 'この名前は既に使われています'));
            } else {
              console.log('test1');
              var otp = Math.random().toString(36).slice(-30);
              user.name = req.body.name;
              user.email = email;
              user.password = bcrypt.hashSync(password, null, null);
              user.is_activated = 0;
              user.created_at = new Date();
              user.updated_at = new Date();
              user.otp = otp;
              return user.save({ returning: 'id' }).then(function (col) {
                var userForMail = {
                  id: user.id,
                  name: user.name,
                  email: user.email,
                  otp: otp
                };
                return done(null, userForMail, req.flash('info', '本登録用のメールを送信しました。\nメールに記載されているURLから本登録を完了させてください'));
              });
            }
          });
        }
      } else {
        return User.findOne({ where: { name: req.body.name } }).then(function (user) {
          if (user) {
            return done(null, false, req.flash('error', 'この名前は既に使われています'));
          } else {
            var otp = Math.random().toString(36).slice(-30);
            var newUser = {
              name: req.body.name,
              email: email,
              password: bcrypt.hashSync(password, null, null),
              is_activated: 0,
              created_at: new Date(),
              updated_at: new Date(),
              otp: otp
            };
            console.log('test2');
            User.create(newUser).then(function (one) {
              console.log(one);
              var userForMail = {
                id: one.get('id'),
                name: one.get('name'),
                email: one.get('email'),
                otp: one.get('otp')
              };
              return done(null, userForMail, req.flash('info', '本登録用のメールを送信しました。\nメールに記載されているURLから本登録を完了させてください'));
            });
          }
        });
      }
    }).catch(function (err) {
      return done(err, req.flash('error', err.message));
    });
  }));

  // Twitter signup

  passport.use('twitter-signup', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField: 'name',
    passwordField: 'email',
    passReqToCallback: true // allows us to pass back the entire request to the callback
  }, function (req, name, email, done) {
    console.log(email);
    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    User.findOne({ where: { email: email } }).then(function (user) {
      if (!!user && user.is_activated === 1) {
        return done(null, false, req.flash('error', 'このメールアドレスは既に使われています'));
      } else {
        return User.findOne({ where: { name: name } }).then(function (user2) {
          if (!!user2 && user2.id !== parseInt(req.body.id, 10)) {
            console.log(user2, parseInt(req.body.id, 10));
            return done(null, false, req.flash('error', 'この名前は既に使われています'));
          } else {
            console.log('test1');
            User.findOne({ where: { id: req.body.id, twitter_id: req.body.twitter_id, otp: req.body.otp } }).then(function (newUser) {
              if (!newUser) {
                return done(null, false, req.flash('error', 'セッションがすでに無効になっています'));
              } else {
                var otp = Math.random().toString(36).slice(-30);
                newUser.otp = otp;
                newUser.name = name;
                newUser.email = email;
                return newUser.save().then(function () {
                  var userForMail = {
                    id: newUser.id,
                    name: newUser.name,
                    email: newUser.email,
                    otp: newUser.otp
                  };
                  return done(null, userForMail, req.flash('info', '本登録用のメールを送信しました。\nメールに記載されているURLから本登録を完了させてください'));
                });
              }
            });
          }
        });
      }
    }).catch(function (err) {
      return done(err, req.flash('error', err.message));
    });
  }));

  // Facebook signup

  passport.use('facebook-signup', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField: 'name',
    passwordField: 'email',
    passReqToCallback: true // allows us to pass back the entire request to the callback
  }, function (req, name, email, done) {
    console.log(email);
    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    User.findOne({ where: { email: email } }).then(function (user) {
      if (!!user && user.is_activated === 1) {
        return done(null, false, req.flash('error', 'このメールアドレスは既に使われています'));
      } else {
        return User.findOne({ where: { name: name } }).then(function (user2) {
          if (!!user2 && user2.id !== parseInt(req.body.id, 10)) {
            console.log(user2, parseInt(req.body.id, 10));
            return done(null, false, req.flash('error', 'この名前は既に使われています'));
          } else {
            console.log('test1');
            User.findOne({ where: { id: req.body.id, facebook_id: req.body.facebook_id, otp: req.body.otp } }).then(function (newUser) {
              if (!newUser) {
                return done(null, false, req.flash('error', 'セッションがすでに無効になっています'));
              } else {
                var otp = Math.random().toString(36).slice(-30);
                newUser.otp = otp;
                newUser.name = name;
                newUser.email = email;
                return newUser.save().then(function () {
                  var userForMail = {
                    id: newUser.id,
                    name: newUser.name,
                    email: newUser.email,
                    otp: newUser.otp
                  };
                  return done(null, userForMail, req.flash('info', '本登録用のメールを送信しました。\nメールに記載されているURLから本登録を完了させてください'));
                });
              }
            });
          }
        });
      }
    }).catch(function (err) {
      return done(err, req.flash('error', err.message));
    });
  }));

  // Google signup

  passport.use('google-signup', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField: 'name',
    passwordField: 'email',
    passReqToCallback: true // allows us to pass back the entire request to the callback
  }, function (req, name, email, done) {
    console.log(email);
    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    User.findOne({ where: { email: email } }).then(function (user) {
      if (!!user && user.is_activated === 1) {
        return done(null, false, req.flash('error', 'このメールアドレスは既に使われています'));
      } else {
        return User.findOne({ where: { name: name } }).then(function (user2) {
          if (!!user2 && user2.id !== parseInt(req.body.id, 10)) {
            console.log(user2, parseInt(req.body.id, 10));
            return done(null, false, req.flash('error', 'この名前は既に使われています'));
          } else {
            console.log('test1');
            User.findOne({ where: { id: req.body.id, google_id: req.body.google_id, otp: req.body.otp } }).then(function (newUser) {
              if (!newUser) {
                return done(null, false, req.flash('error', 'セッションがすでに無効になっています'));
              } else {
                var otp = Math.random().toString(36).slice(-30);
                newUser.otp = otp;
                newUser.name = name;
                newUser.email = email;
                return newUser.save().then(function () {
                  var userForMail = {
                    id: newUser.id,
                    name: newUser.name,
                    email: newUser.email,
                    otp: newUser.otp
                  };
                  return done(null, userForMail, req.flash('info', '本登録用のメールを送信しました。\nメールに記載されているURLから本登録を完了させてください'));
                });
              }
            });
          }
        });
      }
    }).catch(function (err) {
      return done(err, req.flash('error', err.message));
    });
  }));

  // Github signup

  passport.use('github-signup', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField: 'name',
    passwordField: 'email',
    passReqToCallback: true // allows us to pass back the entire request to the callback
  }, function (req, name, email, done) {
    console.log(email);
    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    User.findOne({ where: { email: email } }).then(function (user) {
      if (!!user && user.is_activated === 1) {
        return done(null, false, req.flash('error', 'このメールアドレスは既に使われています'));
      } else {
        return User.findOne({ where: { name: name } }).then(function (user2) {
          if (!!user2 && user2.id !== parseInt(req.body.id, 10)) {
            console.log(user2, parseInt(req.body.id, 10));
            return done(null, false, req.flash('error', 'この名前は既に使われています'));
          } else {
            console.log('test1');
            User.findOne({ where: { id: req.body.id, github_id: req.body.github_id, otp: req.body.otp } }).then(function (newUser) {
              if (!newUser) {
                return done(null, false, req.flash('error', 'セッションがすでに無効になっています'));
              } else {
                var otp = Math.random().toString(36).slice(-30);
                newUser.otp = otp;
                newUser.name = name;
                newUser.email = email;
                return newUser.save().then(function () {
                  var userForMail = {
                    id: newUser.id,
                    name: newUser.name,
                    email: newUser.email,
                    otp: newUser.otp
                  };
                  return done(null, userForMail, req.flash('info', '本登録用のメールを送信しました。\nメールに記載されているURLから本登録を完了させてください'));
                });
              }
            });
          }
        });
      }
    }).catch(function (err) {
      return done(err, req.flash('error', err.message));
    });
  }));

  // LOCAL LOGIN

  passport.use('local-login', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true // allows us to pass back the entire request to the callback
  }, function (req, email, password, done) {
    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    User.findOne({ where: { email: email } }).then(function (user) {
      if (!user) {
        return done(null, false, req.flash('error', 'メールアドレスかパスワードが正しくありません'));
      } else {
        if (user.is_activated === 0) {
          return done(null, false, req.flash('error', '現在仮登録状態のメールアドレスです'));
        } else if (!bcrypt.compareSync(password, user.password)) {
          return done(null, false, req.flash('error', 'メールアドレスかパスワードが正しくありません'));
        } else {
          var authUser = {
            id: user.id,
            name: user.name,
            avatar: user.avatar
          };
          return done(null, authUser, req.flash('info', 'ログインしました'));
        }
      }
    }).catch(function (err) {
      return done(err);
    });
  }));

  // Twitter LOGIN
  passport.use('twitter-login', new TwitterStrategy(straConfig.twitter, function (token, tokenSecret, profile, done) {
    // console.log(token, tokenSecret, profile)
    process.nextTick(function () {
      User.findOne({ where: { twitter_id: profile.id } }).then(function (user) {
        if (user) {
          if (user.is_activated) {
            return done(null, {
              id: user.id,
              name: user.name,
              avatar: user.avatar
            });
          } else {
            var otp = Math.random().toString(36).slice(-30);
            user.name = profile.displayName;
            user.email = profile.email;
            user.twitter_id = user.twitter_id;
            user.is_activated = 0;
            user.created_at = new Date();
            user.updated_at = new Date();
            user.otp = otp;
            user.save().then(function () {
              return done(null, {
                id: user.id,
                name: user.name,
                email: user.email,
                twitter_id: user.twitter_id,
                deactivated: true,
                otp: user.otp,
                newUser: true
              });
            });
          }
        } else {
          var _otp = Math.random().toString(36).slice(-30);
          var newUser = {
            name: profile.displayName,
            email: profile.email,
            twitter_id: profile.id,
            is_activated: 0,
            created_at: new Date(),
            updated_at: new Date(),
            otp: _otp
          };
          User.create(newUser).then(function (res) {
            return done(null, {
              id: res.id,
              name: res.name,
              email: res.email,
              twitter_id: res.twitter_id,
              deactivated: true,
              otp: _otp,
              newUser: true
            });
          });
        }
      });
    });
  }));

  // Facebook LOGIN
  passport.use('facebook-login', new FacebookStrategy(straConfig.facebook, function (token, tokenSecret, profile, done) {
    console.log(profile);
    process.nextTick(function () {
      User.findOne({ where: { facebook_id: profile.id } }).then(function (user) {
        if (user) {
          if (user.is_activated) {
            return done(null, {
              id: user.id,
              name: user.name,
              avatar: user.avatar
            });
          } else {
            var otp = Math.random().toString(36).slice(-30);
            user.name = profile.displayName;
            user.email = profile.emails ? profile.emails[0].value : null;
            user.facebook_id = user.facebook_id;
            user.is_activated = 0;
            user.created_at = new Date();
            user.updated_at = new Date();
            user.otp = otp;
            user.save().then(function () {
              return done(null, {
                id: user.id,
                name: user.name,
                email: user.email,
                facebook_id: user.facebook_id,
                deactivated: true,
                otp: user.otp,
                newUser: true
              });
            });
          }
        } else {
          var _otp2 = Math.random().toString(36).slice(-30);
          var newUser = {
            name: profile.displayName,
            email: profile.emails ? profile.emails[0].value : null,
            facebook_id: profile.id,
            is_activated: 0,
            created_at: new Date(),
            updated_at: new Date(),
            otp: _otp2
          };
          console.log('newUser', newUser);
          User.create(newUser).then(function (res) {
            return done(null, {
              id: res.id,
              name: res.name,
              email: res.email,
              facebook_id: res.facebook_id,
              deactivated: true,
              otp: _otp2,
              newUser: true
            });
          });
        }
      });
    });
  }));

  // Google LOGIN
  passport.use('google-login', new GoogleStrategy(straConfig.google, function (token, tokenSecret, profile, done) {
    console.log(profile);
    process.nextTick(function () {
      User.findOne({ where: { google_id: profile.id } }).then(function (user) {
        if (user) {
          if (user.is_activated) {
            return done(null, {
              id: user.id,
              name: user.name,
              email: user.email,
              avatar: user.avatar
            });
          } else {
            var otp = Math.random().toString(36).slice(-30);
            user.name = profile.displayName;
            user.email = profile.emails ? profile.emails[0].value : null;
            user.google_id = user.google_id;
            user.is_activated = 0;
            user.created_at = new Date();
            user.updated_at = new Date();
            user.otp = otp;
            user.save().then(function () {
              return done(null, {
                id: user.id,
                name: user.name,
                email: user.email,
                google_id: user.google_id,
                deactivated: true,
                otp: user.otp,
                newUser: true
              });
            });
          }
        } else {
          var _otp3 = Math.random().toString(36).slice(-30);
          var newUser = {
            name: profile.displayName,
            email: profile.emails ? profile.emails[0].value : null,
            google_id: profile.id,
            is_activated: 0,
            created_at: new Date(),
            updated_at: new Date(),
            otp: _otp3
          };
          console.log('newUser', newUser);
          User.create(newUser).then(function (res) {
            return done(null, {
              id: res.id,
              name: res.name,
              email: res.email,
              google_id: res.google_id,
              deactivated: true,
              otp: _otp3,
              newUser: true
            });
          });
        }
      });
    });
  }));

  // Github LOGIN
  passport.use('github-login', new GithubStrategy(straConfig.github, function (token, tokenSecret, profile, done) {
    console.log(profile);
    process.nextTick(function () {
      User.findOne({ where: { github_id: profile.id } }).then(function (user) {
        if (user) {
          if (user.is_activated) {
            return done(null, {
              id: user.id,
              name: user.name,
              avatar: user.avatar
            });
          } else {
            var otp = Math.random().toString(36).slice(-30);
            user.name = profile.displayName;
            user.email = profile.emails ? profile.emails[0].value : null;
            user.github_id = user.github_id;
            user.is_activated = 0;
            user.created_at = new Date();
            user.updated_at = new Date();
            user.otp = otp;
            user.save().then(function () {
              return done(null, {
                id: user.id,
                name: user.name,
                email: user.email,
                github_id: user.github_id,
                deactivated: true,
                otp: user.otp,
                newUser: true
              });
            });
          }
        } else {
          var _otp4 = Math.random().toString(36).slice(-30);
          var newUser = {
            name: profile.displayName,
            email: profile.emails ? profile.emails[0].value : null,
            github_id: profile.id,
            is_activated: 0,
            created_at: new Date(),
            updated_at: new Date(),
            otp: _otp4
          };

          User.create(newUser).then(function (res) {
            return done(null, {
              id: res.id,
              name: res.name,
              email: res.email,
              github_id: res.github_id,
              deactivated: true,
              otp: _otp4,
              newUser: true
            });
          });
        }
      });
    });
  }));
};

/***/ }),
/* 23 */
/***/ (function(module, exports) {

module.exports = require("passport-local");

/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = require("passport-twitter");

/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = require("passport-facebook");

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = require("passport-google-oauth");

/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = require("passport-github2");

/***/ }),
/* 28 */
/***/ (function(module, exports) {

var twitter = {
  consumerKey: 'o2dc1yXwsMAudUcSWwGBwFUG7',
  consumerSecret: 'cURqHyl4bBYdlymQBUmQ5twX3GpUuPHWAVblOXRbTcuWxpkWHQ',
  userProfileURL: 'https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true',
  callbackURL: 'http://localhost:3000/api/callback/twitter' // Twitterログイン後、遷移するURL
};

var facebook = {
  clientID: '359230551157627',
  clientSecret: 'faff1fed47864335a15687fff40d49cf',
  callbackURL: 'http://localhost:3000/api/callback/facebook',
  profileFields: ['id', 'displayName', 'email']
};

var google = {
  clientID: '607136510374-g2d9b1kmr3ne3iakapvaj3j4dnoprdrs.apps.googleusercontent.com',
  clientSecret: 'uqMDwWsRboACKGMOhdzdKDQy',
  callbackURL: 'http://localhost:3000/api/callback/google',
  profileFields: ['id', 'displayName', 'email']
};

var github = {
  clientID: '6c86aa0bed4477af48db',
  clientSecret: '3ddaa8cfe17511af47c99c7d6895177fa68f5af8',
  callbackURL: 'http://localhost:3000/api/callback/github',
  profileFields: ['id', 'displayName', 'email']
};

module.exports = {
  twitter: twitter,
  facebook: facebook,
  google: google,
  github: github
};

/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'cool_hacks',
    meta: [{ charset: 'utf-8' }, { name: 'viewport', content: 'width=device-width, initial-scale=1' }, { hid: 'description', name: 'description', content: 'Cool Hacks' }],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }, {
      rel: 'stylesheet',
      href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
    }, {
      rel: 'stylesheet',
      href: 'https://fonts.googleapis.com/css?family=Orbitron'
    }]
  },
  plugins: ['~/plugins/vuetify.js', { src: '~/plugins/moment.js', ssr: false }, { src: '~/plugins/vue_youtube_embed.js', ssr: false }, { src: '~/plugins/vuevideoplayer.js' }],
  css: ['~/assets/style/app.styl'],
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    vendor: ['vuetify', 'axios', 'moment'],
    extractCSS: true,
    /*
    ** Run ESLINT on save
    */
    extend: function extend(config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
    }
  }
};

/***/ })
/******/ ]);
//# sourceMappingURL=main.map