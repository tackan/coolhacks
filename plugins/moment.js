import _ from 'lodash'
import vueMoment from 'vue-moment'
import Vue from 'vue'
const moment = require('moment')

let locale = 'ja' // default

const navigator = _.get(window, 'navigator', {})
locale = (_.head(navigator.languages) || navigator.language || navigator.browserLanguage || navigator.userLanguage).substr(0, 2)
moment.locale(locale)

Vue.use(vueMoment, { moment })
