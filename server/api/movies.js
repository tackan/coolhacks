import { Router } from 'express'
const models = require('./../../models')
const router = Router()

/* GET movies listing. */
router.get('/movies', (req, res, next) => {
  models.movie
    .findAll({
      include: [
        { model: models.user, attributes: ['name', 'avatar'] },
        { model: models.tag, attributes: ['id', 'tag_name'] }
      ],
      limit: 12,
      offset: 0
    })
    .then(m => {
      res.json(m)
    })
    .catch(e => {
      res.json(e.meesage)
    })
})

/* GET movies/search listing. */
router.get('/movies/search', (req, res, next) => {
  let word = ''
  let limit
  let offset
  if (req.query.search_word) {
    word = req.query.search_word
  }

  if (req.query.begin) {
    offset = req.query.begin > 0 ? req.query.begin - 1 : 0
  } else {
    offset = 0
  }

  if (req.query.limit) {
    limit = parseInt(req.query.limit)
    limit = limit > 0 ? limit : 12
  } else {
    limit = 12
  }

  models.movie
    .findAll({
      include: [
        {
          model: models.user,
          attributes: ['id', 'name', 'avatar']
        },
        {
          model: models.tag,
          attributes: ['id', 'tag_name']
        }
      ],
      limit: limit,
      offset: offset,
      where: {
        [models.Sequelize.Op.or]: [
          {
            title: {
              [models.Sequelize.Op.iLike]: `%${word}%`
            }
          },
          {
            description: {
              [models.Sequelize.Op.iLike]: `%${word}%`
            }
          }
        ]
      },
      order: [['created_at', 'DESC']]
    })
    .then(movies => {
      res.json(movies)
    })
})

/* GET movies/recomended listing. */
router.get('/movies/recomendeds', (req, res, next) => {
  let limit
  let offset
  if (req.query.begin) {
    offset = req.query.begin > 0 ? req.query.begin - 1 : 0
  } else {
    offset = 0
  }
  if (req.query.limit) {
    limit = parseInt(req.query.limit)
    limit = limit > 0 ? limit : 12
  } else {
    limit = 12
  }

  models.movie
    .findAll({
      include: [
        { model: models.user, attributes: ['name', 'avatar'] },
        { model: models.tag, attributes: ['id', 'tag_name'] }
      ],
      limit: limit,
      offset: offset,
      order: [['w_count', 'DESC'], ['c_count', 'DESC']]
    })
    .then(m => {
      res.json(m)
    })
    .catch(e => {
      res.json(e.meesage)
    })
})

/* GET movies/recomended listing. */
router.get('/movies/news', (req, res, next) => {
  let limit
  let offset
  if (req.query.begin) {
    offset = req.query.begin > 0 ? req.query.begin - 1 : 0
  } else {
    offset = 0
  }
  if (req.query.limit) {
    limit = parseInt(req.query.limit)
    limit = limit > 0 ? limit : 12
  } else {
    limit = 12
  }

  models.movie
    .findAll({
      include: [
        { model: models.user, attributes: ['name', 'avatar'] },
        { model: models.tag, attributes: ['id', 'tag_name'] }
      ],
      limit: limit,
      offset: offset,
      order: [['created_at', 'DESC']]
    })
    .then(m => {
      res.json(m)
    })
    .catch(e => {
      res.json(e.meesage)
    })
})

/* GET movie by ID. */
router.get('/movie/:id', (req, res, next) => {
  let id = parseInt(req.params.id)
  if (id) {
    models.movie
      .findOne({
        where: { id: id },
        include: [
          {
            model: models.user,
            attributes: ['name', 'avatar']
          },
          {
            model: models.tag,
            attributes: ['id', 'tag_name']
          },
          {
            model: models.comment,
            attributes: ['id', 'text', 'timing', 'is_question', 'goods'],
            include: [
              {
                model: models.answer,
                attributes: ['id', 'text', 'question_id', 'updated_at'],
                include: [
                  {
                    model: models.user,
                    attributes: ['name', 'avatar']
                  }
                ]
              },
              {
                model: models.user,
                attributes: ['name', 'avatar']
              }
            ]
          }
        ],
        order: [
          [models.comment, 'timing', 'ASC'],
          [models.comment, models.answer, 'updated_at', 'ASC']
        ]
      })
      .then(m => {
        res.json(m)
      })
      .catch(e => {
        console.log(e.message)
        res.json(e.meesage)
      })
  } else {
    res.sendStatus(404)
  }
})

router.get('/movie/:id/comments', (req, res, next) => {
  let id = parseInt(req.params.id)
  if (id) {
    models.comment
      .findAll({
        where: { movie_id: id },
        attributes: ['id', 'text', 'timing', 'is_question', 'goods'],
        include: [
          {
            model: models.answer,
            attributes: ['id', 'text', 'question_id', 'updated_at'],
            include: [
              {
                model: models.user,
                attributes: ['name', 'avatar']
              }
            ]
          },
          {
            model: models.user,
            attributes: ['name', 'avatar']
          }
        ],
        order: [['timing', 'ASC'], [models.answer, 'updated_at', 'ASC']]
      })
      .then(m => {
        console.log(m)
        res.json(m)
      })
      .catch(e => {
        res.json(e.message)
      })
  } else {
    res.sendStatus(404)
  }
})

router.post('/movie/:id/comment', (req, res, next) => {
  let movieId = parseInt(req.params.id)
  req.body.text = req.body.text.replace(/^\s+|\s+$/g, '')
  if (
    movieId &&
    req.session.authUser &&
    req.session.authUser.id === req.body.user_id &&
    req.body.text
  ) {
    let comment = req.body
    comment.movie_id = movieId
    models.comment
      .create(comment)
      .then(m => {
        models.comment
          .findAll({
            where: { movie_id: movieId },
            include: [
              {
                model: models.answer,
                attributes: ['text', 'question_id', 'updated_at'],
                include: [
                  {
                    model: models.user,
                    attributes: ['name', 'avatar']
                  }
                ]
              },
              {
                model: models.user,
                attributes: ['name', 'avatar']
              }
            ],
            order: [['timing', 'ASC'], [models.answer, 'updated_at', 'ASC']]
          })
          .then(m => {
            res.json(m)
          })
          .catch(e => {
            res.json(e.message)
          })
      })
      .catch(e => {
        res.json(e.message)
      })
  } else {
    models.comment
      .findAll({
        where: { movie_id: movieId },
        include: [
          {
            model: models.answer,
            attributes: ['text', 'question_id', 'updated_at'],
            include: [
              {
                model: models.user,
                attributes: ['name', 'avatar']
              }
            ]
          },
          {
            model: models.user,
            attributes: ['name', 'avatar']
          }
        ],
        order: [['timing', 'ASC'], [models.answer, 'updated_at', 'ASC']]
      })
      .then(m => {
        res.json(m)
      })
      .catch(e => {
        res.json(e.message)
      })
  }
})

router.get('/movie/:id/cools', (req, res, next) => {
  let id = parseInt(req.params.id)
  if (id) {
    models.coolaggregate
      .findAll({
        where: { id: id },
        attributes: ['time_id', 'count'],
        order: [['time_id', 'ASC']]
      })
      .then(m => {
        res.json(m)
      })
      .catch(e => {
        res.json(e.meesage)
      })
  } else {
    res.sendStatus(404)
  }
})

router.get('/movie/:id/nexts', (req, res, next) => {
  // タグが一致していて且つ閲覧数とcoolカウント上位20件
  let id = parseInt(req.params.id)
  if (id) {
    models.movie
      .findOne({
        where: {
          id: id
        },
        attributes: [],
        include: [
          {
            model: models.tag,
            attributes: ['id']
          }
        ]
      })
      .then(m => {
        let ids = m.tags.map(tag => {
          return tag.id
        })
        models.movie
          .findAll({
            attributes: ['id', 'w_count', 'c_count'],
            include: [
              {
                model: models.tag,
                attributes: ['id'],
                where: {
                  id: {
                    [models.Sequelize.Op.in]: ids
                  }
                }
              }
            ],
            where: {
              id: {
                [models.Sequelize.Op.not]: id
              }
            },
            limit: 20,
            offset: 0,
            order: [['w_count', 'DESC'], ['c_count', 'DESC']]
          })
          .then(idRes => {
            console.log(idRes)
            let mIds = idRes.map(val => {
              return val.id
            })
            models.movie
              .findAll({
                include: [
                  {
                    model: models.user,
                    attributes: ['name', 'avatar']
                  },
                  {
                    model: models.tag,
                    attributes: ['id', 'tag_name']
                  }
                ],
                where: {
                  id: {
                    [models.Sequelize.Op.in]: mIds
                  }
                },
                limit: 20,
                offset: 0,
                order: [['w_count', 'DESC'], ['c_count', 'DESC']]
              })
              .then(result => {
                console.log(result)
                res.json(result)
              })
          })
      })
      .catch(e => {
        res.json(e.meesage)
      })
  } else {
    res.sendStatus(404)
  }
})

router.post('/movie/:id/cool', (req, res, next) => {
  let cool = req.body.cool
  cool.movie_id = req.params.id
  if (cool.user_id) {
    console.log('LOGGEDIN USER')
  } else {
    console.log('GUEST USER')
  }
  models.cool
    .create(cool)
    .then(result => {
      res.json({ info: 'Cool posted!' })
    })
    .catch(e => {
      res.json({ error: e.message })
    })
})

router.post('/movie/:id/answer', (req, res, next) => {
  let movieId = parseInt(req.params.id)
  req.body.text = req.body.text.replace(/^\s+|\s+$/g, '')
  if (
    movieId &&
    req.session.authUser &&
    req.session.authUser.id === req.body.user_id &&
    req.body.text
  ) {
    let answer = req.body
    answer.movie_id = movieId
    models.answer
      .create(answer)
      .then(m => {
        models.comment
          .findAll({
            where: { movie_id: movieId },
            include: [
              {
                model: models.answer,
                attributes: ['text', 'question_id', 'updated_at'],
                include: [
                  {
                    model: models.user,
                    attributes: ['name', 'avatar']
                  }
                ],
                order: [['updated_at', 'ASC']]
              },
              {
                model: models.user,
                attributes: ['name', 'avatar']
              }
            ],
            order: [['timing', 'ASC'], [models.answer, 'updated_at', 'ASC']]
          })
          .then(m => {
            res.json(m)
          })
          .catch(e => {
            res.json(e.message)
          })
      })
      .catch(e => {
        res.json(e.message)
      })
  } else {
    models.comment
      .findAll({
        where: { movie_id: movieId },
        include: [
          {
            model: models.answer,
            attributes: ['text', 'question_id', 'updated_at'],
            include: [
              {
                model: models.user,
                attributes: ['name', 'avatar']
              }
            ],
            order: [['updated_at', 'ASC']]
          },
          {
            model: models.user,
            attributes: ['name', 'avatar']
          }
        ],
        order: [['timing', 'ASC'], [models.answer, 'updated_at', 'ASC']]
      })
      .then(m => {
        res.json(m)
      })
      .catch(e => {
        res.json(e.message)
      })
  }
})

export default router
