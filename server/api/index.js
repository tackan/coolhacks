import { Router } from 'express'

import movies from './movies'
import session from './session'

const router = Router()

// Add USERS Routes
router.use(movies)
router.use(session)

export default router
