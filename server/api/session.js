import passport from 'passport'
import { Router } from 'express'
const mailer = require('./../mailer/mailer')
let url = require('url')
const router = Router()
const bcrypt = require('bcrypt-nodejs')

const User = require('./../../models').user

// ====================
// ====== Local =======
// ====================

router.get('/signup/:id/:otp', (req, res, next) => {
  User.findOne({ where: { id: req.params.id, otp: req.params.otp, is_activated: 0 } })
    .then((user) => {
      if (!user) {
        req.flash('error', '無効なリンクです。初めからやり直してください')
        res.redirect('/')
      } else {
        if (user.updated_at.getTime() + 1000 * 60 * 60 * 24 <= new Date().getTime()) {
          req.flash('error', 'すでに無効になっているリンクです。初めからやり直してください')
          res.redirect('/')
        } else {
          user.update({ is_activated: 1, updated_at: new Date(), otp: null })
            .then(() => {
              let authUser = {
                id: user.id,
                name: user.name,
                avatar: user.avatar
              }
              req.session.authUser = authUser
              req.flash('success', '本登録完了！')
              res.redirect('/')
            })
        }
      }
    })
    .catch((err) => {
      console.log(err.message)
      req.flash('error', '無効なリンクです。初めからやり直してください')
      res.redirect('/')
    })
})

router.post('/signup', (req, res, next) => {
  passport.authenticate('local-signup', (err, user, info) => {
    if (err) {
      return res.json(
        {
          error: err.message
          // activateMailSended: false
        }
      )
    }
    if (!user) {
      return res.json(
        {
          error: req.flash('error')[0]
          // activateMailSended: false
        }
      )
    } else {
      // console.log(mailer.sendActivateMessage)
      mailer.sendActivateMessage(user, (success) => {
        if (success) {
          return res.json(
            {
              info: '本登録用メールを送信しました。\nメールに記載されているリンクから登録を完了させてください'
              // activateMailSended: false
            }
          )
        } else {
          return res.json(
            {
              error: 'メールを送信できませんでした。もう一度試してみてください'
              // activateMailSended: false
            }
          )
        }
      })
    }
  })(req, res, next)
})

router.post('/login', (req, res, next) => {
  passport.authenticate('local-login', (err, user, info) => {
    if (err) {
      return res.json({
        error: err.message
      })
    } else if (!user) {
      return res.json({
        error: 'メールアドレスかパスワードが正しくありません'
      })
    } else {
      req.session.authUser = user

      return res.json(
        {
          info: 'ログインしました',
          user: user
        }
      )
    }
  })(req, res, next)
})

// ====================
// ===== Twitter ======
// ====================

router.get('/callback/twitter', (req, res, next) => {
  console.log('twitter callback0')
  passport.authenticate('twitter-login', (err, user, info) => {
    console.log('twitter callback')
    if (err) {
      console.log(err.message)
      return res.json({
        error: err.message
      })
    } else if (!user) {
      return res.json({
        error: 'ログインエラー。twitterアカウントが見つかりませんでした'
      })
    } else {
      if (user.deactivated) {
        console.log(user)
        return res.redirect(url.format({
          pathname: req.session.current_path,
          query: user
        })
        )
        // let path = `${req.session.current_path}?id=${user.id}&name=${user.name}&email=${user.email}&newUser=${true}`
        // return res.redirect(path)
      } else {
        req.session.authUser = user
        req.flash('info', 'ログインしました。')
        return res.redirect(req.session.current_path)
      }
      // return res.json(user)
    }
  })(req, res, next)
})

router.get('/twitter', (req, res, next) => {
  console.log('twitter0')
  req.session.current_path = req.query.current_path
  console.log(req.query)
  passport.authenticate('twitter-login', (err, user, info) => {
    console.log('twitter')
    if (err) {
      console.log(err.message)
      return res.json({
        error: err.message
      })
    }
  })(req, res, next)
})

router.post('/twitter/activate', (req, res, next) => {
  passport.authenticate('twitter-signup', (err, user, info) => {
    if (err) {
      return res.json(
        {
          error: err.message
          // activateMailSended: false
        }
      )
    }
    if (!user) {
      return res.json(
        {
          error: req.flash('error')[0]
          // activateMailSended: false
        }
      )
    } else {
      // console.log(mailer.sendActivateMessage)
      mailer.sendActivateMessageTwitter(user, (success) => {
        if (success) {
          return res.json(
            {
              info: '本登録用メールを送信しました。\nメールに記載されているリンクから登録を完了させてください'
              // activateMailSended: false
            }
          )
        } else {
          return res.json(
            {
              error: 'メールを送信できませんでした。もう一度試してみてください'
              // activateMailSended: false
            }
          )
        }
      })
    }
  })(req, res, next)
})

router.get('/twitter/activate/:id/:otp', (req, res, next) => {
  User.findOne({ where: { id: req.params.id, otp: req.params.otp, is_activated: 0 } })
    .then((user) => {
      if (!user) {
        req.flash('error', '無効なリンクです。初めからやり直してください')
        res.redirect('/')
      } else {
        if (user.updated_at.getTime() + 1000 * 60 * 60 * 24 <= new Date().getTime()) {
          req.flash('error', 'すでに無効になっているリンクです。初めからやり直してください')
          res.redirect('/')
        } else {
          user.update({ is_activated: 1, updated_at: new Date(), otp: null }).then(() => {
            let authUser = {
              id: user.id,
              name: user.name,
              avatar: user.avatar
            }
            req.session.authUser = authUser
            req.flash('success', '本登録完了！')
            res.redirect('/')
          })
        }
      }
    })
    .catch((err) => {
      console.log(err.message)
      req.flash('error', '無効なリンクです。初めからやり直してください')
      res.redirect('/')
    })
})

// ====================
// ===== Facebook =====
// ====================
router.get('/callback/facebook', (req, res, next) => {
  console.log('facebook callback0')
  passport.authenticate('facebook-login', (err, user, info) => {
    console.log('facebook callback')
    if (err) {
      console.log(err.message)
      return res.json({
        error: err.message
      })
    } else if (!user) {
      return res.json({
        error: 'ログインエラー。facebookアカウントが見つかりませんでした'
      })
    } else {
      if (user.deactivated) {
        console.log(user)
        return res.redirect(url.format({
          pathname: req.session.current_path,
          query: user
        })
        )
      } else {
        req.session.authUser = user
        req.flash('info', 'ログインしました。')
        return res.redirect(req.session.current_path)
      }
    }
  })(req, res, next)
})

router.get('/facebook', (req, res, next) => {
  console.log('facebook0')
  req.session.current_path = req.query.current_path
  console.log(req.query)
  passport.authenticate('facebook-login', (err, user, info) => {
    console.log('facebook')
    if (err) {
      console.log(err.message)
      return res.json({
        error: err.message
      })
    }
  })(req, res, next)
})

router.post('/facebook/activate', (req, res, next) => {
  passport.authenticate('facebook-signup', (err, user, info) => {
    if (err) {
      return res.json(
        {
          error: err.message
          // activateMailSended: false
        }
      )
    }
    if (!user) {
      return res.json(
        {
          error: req.flash('error')[0]
          // activateMailSended: false
        }
      )
    } else {
      // console.log(mailer.sendActivateMessage)
      mailer.sendActivateMessageFacebook(user, (success) => {
        if (success) {
          return res.json(
            {
              info: '本登録用メールを送信しました。\nメールに記載されているリンクから登録を完了させてください'
              // activateMailSended: false
            }
          )
        } else {
          return res.json(
            {
              error: 'メールを送信できませんでした。もう一度試してみてください'
              // activateMailSended: false
            }
          )
        }
      })
    }
  })(req, res, next)
})

router.get('/facebook/activate/:id/:otp', (req, res, next) => {
  User.findOne({ where: { id: req.params.id, otp: req.params.otp, is_activated: 0 } })
    .then((user) => {
      if (!user) {
        req.flash('error', '無効なリンクです。初めからやり直してください')
        res.redirect('/')
      } else {
        if (user.updated_at.getTime() + 1000 * 60 * 60 * 24 <= new Date().getTime()) {
          req.flash('error', 'すでに無効になっているリンクです。初めからやり直してください')
          res.redirect('/')
        } else {
          user.update({ is_activated: 1, updated_at: new Date(), otp: null }).then(() => {
            let authUser = {
              id: user.id,
              name: user.name,
              avatar: user.avatar
            }
            req.session.authUser = authUser
            req.flash('success', '本登録完了！')
            res.redirect('/')
          })
        }
      }
    })
    .catch((err) => {
      console.log(err.message)
      req.flash('error', '無効なリンクです。初めからやり直してください')
      res.redirect('/')
    })
})

// ====================
// ====== Google ======
// ====================
router.get('/callback/google', (req, res, next) => {
  console.log('google callback0')
  passport.authenticate('google-login', (err, user, info) => {
    console.log('google callback')
    if (err) {
      console.log(err.message)
      return res.json({
        error: err.message
      })
    } else if (!user) {
      return res.json({
        error: 'ログインエラー。googleアカウントが見つかりませんでした'
      })
    } else {
      if (user.deactivated) {
        console.log(user)
        return res.redirect(url.format({
          pathname: req.session.current_path,
          query: user
        })
        )
      } else {
        req.session.authUser = user
        req.flash('info', 'ログインしました。')
        return res.redirect(req.session.current_path)
      }
    }
  })(req, res, next)
})

router.get('/google', (req, res, next) => {
  console.log('google0')
  req.session.current_path = req.query.current_path
  console.log(req.query)
  passport.authenticate('google-login', { scope: ['https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/plus.profile.emails.read'] }, (err, user, info) => {
    console.log('google')
    if (err) {
      console.log(err.message)
      return res.json({
        error: err.message
      })
    }
  })(req, res, next)
})

router.post('/google/activate', (req, res, next) => {
  passport.authenticate('google-signup', (err, user, info) => {
    if (err) {
      return res.json(
        {
          error: err.message
          // activateMailSended: false
        }
      )
    }
    if (!user) {
      return res.json(
        {
          error: req.flash('error')[0]
          // activateMailSended: false
        }
      )
    } else {
      // console.log(mailer.sendActivateMessage)
      mailer.sendActivateMessageGoogle(user, (success) => {
        if (success) {
          return res.json(
            {
              info: '本登録用メールを送信しました。\nメールに記載されているリンクから登録を完了させてください'
              // activateMailSended: false
            }
          )
        } else {
          return res.json(
            {
              error: 'メールを送信できませんでした。もう一度試してみてください'
              // activateMailSended: false
            }
          )
        }
      })
    }
  })(req, res, next)
})

router.get('/google/activate/:id/:otp', (req, res, next) => {
  User.findOne({ where: { id: req.params.id, otp: req.params.otp, is_activated: 0 } })
    .then((user) => {
      if (!user) {
        req.flash('error', '無効なリンクです。初めからやり直してください')
        res.redirect('/')
      } else {
        if (user.updated_at.getTime() + 1000 * 60 * 60 * 24 <= new Date().getTime()) {
          req.flash('error', 'すでに無効になっているリンクです。初めからやり直してください')
          res.redirect('/')
        } else {
          user.update({ is_activated: 1, updated_at: new Date(), otp: null }).then(() => {
            let authUser = {
              id: user.id,
              name: user.name,
              avatar: user.avatar
            }
            req.session.authUser = authUser
            req.flash('success', '本登録完了！')
            res.redirect('/')
          })
        }
      }
    })
    .catch((err) => {
      console.log(err.message)
      req.flash('error', '無効なリンクです。初めからやり直してください')
      res.redirect('/')
    })
})

// ====================
// ====== Github ======
// ====================
router.get('/callback/github', (req, res, next) => {
  console.log('github callback0')
  passport.authenticate('github-login', (err, user, info) => {
    console.log('github callback')
    if (err) {
      console.log(err.message)
      return res.json({
        error: err.message
      })
    } else if (!user) {
      return res.json({
        error: 'ログインエラー。githubアカウントが見つかりませんでした'
      })
    } else {
      if (user.deactivated) {
        console.log(user)
        return res.redirect(url.format({
          pathname: req.session.current_path,
          query: user
        })
        )
      } else {
        req.session.authUser = user
        req.flash('info', 'ログインしました。')
        return res.redirect(req.session.current_path)
      }
    }
  })(req, res, next)
})

router.get('/github', (req, res, next) => {
  console.log('github0')
  req.session.current_path = req.query.current_path
  console.log(req.query)
  passport.authenticate('github-login', { scope: ['user:email'] }, (err, user, info) => {
    console.log('github')
    if (err) {
      console.log(err.message)
      return res.json({
        error: err.message
      })
    }
  })(req, res, next)
})

router.post('/github/activate', (req, res, next) => {
  passport.authenticate('github-signup', (err, user, info) => {
    if (err) {
      return res.json(
        {
          error: err.message
          // activateMailSended: false
        }
      )
    }
    if (!user) {
      return res.json(
        {
          error: req.flash('error')[0]
          // activateMailSended: false
        }
      )
    } else {
      // console.log(mailer.sendActivateMessage)
      mailer.sendActivateMessageGithub(user, (success) => {
        if (success) {
          return res.json(
            {
              info: '本登録用メールを送信しました。\nメールに記載されているリンクから登録を完了させてください'
              // activateMailSended: false
            }
          )
        } else {
          return res.json(
            {
              error: 'メールを送信できませんでした。もう一度試してみてください'
              // activateMailSended: false
            }
          )
        }
      })
    }
  })(req, res, next)
})

router.get('/github/activate/:id/:otp', (req, res, next) => {
  User.findOne({ where: { id: req.params.id, otp: req.params.otp, is_activated: 0 } })
    .then((user) => {
      if (!user) {
        req.flash('error', '無効なリンクです。初めからやり直してください')
        res.redirect('/')
      } else {
        if (user.updated_at.getTime() + 1000 * 60 * 60 * 24 <= new Date().getTime()) {
          req.flash('error', 'すでに無効になっているリンクです。初めからやり直してください')
          res.redirect('/')
        } else {
          user.update({ is_activated: 1, updated_at: new Date(), otp: null }).then(() => {
            let authUser = {
              id: user.id,
              name: user.name,
              avatar: user.avatar
            }
            req.session.authUser = authUser
            req.flash('success', '本登録完了！')
            res.redirect('/')
          })
        }
      }
    })
    .catch((err) => {
      console.log(err.message)
      req.flash('error', '無効なリンクです。初めからやり直してください')
      res.redirect('/')
    })
})

// ====================
// == Password Reset ==
// ====================

router.post('/forgot', (req, res, next) => {
  console.log(req.body)
  if (!req.body.email) {
    // res.json({
    //   error: '通信エラー'
    // })
    req.flash(
      'error', '通信エラー'
    )
  } else {
    console.log(req.body.email)
    User.findOne({ where: { email: req.body.email, is_activated: 1 } }).then((user) => {
      console.log(user)
      if (!user) {
        return res.json({
          error: '登録されていないメールアドレスです'
        })
      } else if (!user.password) {
        let errMsg
        if (user.twitter_id) {
          errMsg = 'すでにTwitterアカウントでの登録があります'
        } else if (user.facebook_id) {
          errMsg = 'すでにFacebookアカウントでの登録があります'
        } else if (user.google_id) {
          errMsg = 'すでにGoogleアカウントでの登録があります'
        } else if (user.github_id) {
          errMsg = 'すでにGithubアカウントでの登録があります'
        }
        return res.json({
          error: errMsg
        })
      } else {
        console.log('user exist')
        console.log(user)
        let token = Math.random().toString(36).slice(-30)
        let expire = new Date()
        expire.setHours(expire.getHours() + 3)
        return user.update({ reset_pass_token: token, reset_pass_expire: expire }).then(() => {
          console.log('update')
          let userForMail = {
            id: user.id,
            name: user.name,
            email: user.email,
            reset_pass_token: token
          }
          mailer.sendPasswordResetMessage(userForMail, (success) => {
            if (success) {
              console.log('success')
              return res.json(
                {
                  info: 'パスワードリセット用メールを送信しました。\nメールに記載されているリンクから再登録を完了させてください'
                  // activateMailSended: false
                }
              )
            } else {
              console.log('false')
              return res.json(
                {
                  error: 'メールを送信できませんでした。もう一度試してみてください'
                  // activateMailSended: false
                }
              )
            }
          })
        })
      }
    }).catch((err) => {
      return res.json({ error: err.message })
    })
  }
})

router.post('/reset', (req, res, next) => {
  console.log(req.body)
  User.findOne({ where: { id: parseInt(req.body.id), reset_pass_token: req.body.reset_pass_token } })
    .then((user) => {
      if (!user) {
        console.log('no user')
        res.json({
          error: '無効なリンクです。初めからやり直してください'
        })
        // req.flash('error', '無効なリンクです。初めからやり直してください')
        // res.json({})
      } else {
        if (user.reset_pass_expire.getTime() <= new Date().getTime()) {
          console.log('invalid link')
          res.json({
            error: 'すでに無効になっているリンクです。初めからやり直してください'
          })
          // req.flash('error', 'すでに無効になっているリンクです。初めからやり直してください')
          // res.json({})
        } else {
          console.log('user exist')
          user.update({ password: bcrypt.hashSync(req.body.password, null, null), updated_at: new Date(), reset_pass_token: null, reset_pass_expire: null }).then(() => {
            res.json({
              info: 'パスワードの再設定が完了しました。元のページに戻ってログインしてください'
            })
            // req.flash('info', 'パスワードの再設定が完了しました。元のページに戻ってログインしてください')
            // res.json({})
          })
        }
      }
    })
    .catch((err) => {
      console.log(err.message)
      res.json({
        error: '無効なリンクです。初めからやり直してください'
      })
      // req.flash('error', '無効なリンクです。初めからやり直してください')
      // res.json({})
    })
})

router.post('/logout', (req, res, next) => {
  delete req.session.authUser
  res.json({ info: 'ログアウトしました' })
})

export default router
