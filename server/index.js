import express from 'express'
import session from 'express-session'
// import ejwt from 'express-jwt'

import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import morgan from 'morgan'
import passport from 'passport'
import flash from 'connect-flash'
import { Nuxt, Builder } from 'nuxt'

import api from './api'
import secret from './config/secret'
require('./config/passport')(passport)

const app = express()
const host = process.env.HOST || '127.0.0.1'
const port = process.env.PORT || 3000

app.set('port', port)

// set up application
app.use(morgan('dev'))
app.use(cookieParser()) // read cookies (needed for auth)
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
// app.use(ejwt({ secret: secret.secret, userProperty: 'tokenPayload' }).unless({ path: ['/api/login', '/api/movies/search', 'api/movies/news', 'api/movies/recomended', '/'] }))
app.use(session(secret))
app.use(passport.initialize())
app.use(passport.session())
app.use(flash())
// Import API Routes
app.use('/api', api)

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

// Init Nuxt.js
const nuxt = new Nuxt(config)

// Build only in dev mode
if (config.dev) {
  const builder = new Builder(nuxt)
  builder.build()
}

// Give nuxt middleware to express
app.use(nuxt.render)

// Listen the server
app.listen(port, host)
console.log('ENV: ' + process.env.NODE_ENV)
console.log('Server listening on ' + host + ':' + port) // eslint-disable-line no-console
